Push Vendor API
===============

PesonaEdu Store will push data as described below when payment for order is success


1. Request
-------

### 1.1 Protocol

Communication using standard HTTP/1.1

### 1.1 Type

Data will be included inside `body` on HTTP Request using `POST` method

### 1.2 Parameter

Key             | Description
--------------- | -------------------
price           | Product price
order\_number   | The number of order
product\_code   | Product code
qty             | Quantity bought
options         | Product's variants
email           | Email of user
unique\_id      | Unique id

### 1.3

Response type must follow *HTTP Response for Vendor Services* see `vendor-response.md` for further information

