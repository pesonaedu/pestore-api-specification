API Response error Code
=======================

This document describes error code returned by PesonaEdu Store API

Code | Message                         | Description                                    | API URL
---- | ------------------------------- | ---------------------------------------------- | ----------------
101  | Username/password wrong!        | Username or password sent from client is wrong | /api/login
201  | No data                         | No data from database                          | /api/promotions