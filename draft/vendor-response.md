HTTP Response for Vendor Services
=================================

This document describes response specification for vendor when PesonaEdu-Store
system call vendor's web services


1. Protocol
-----------

Communication using standard HTTP/1.1

See [RFC 2616]


2. Response
----------------

### 2.1 Type

Response should always follow [JSON] format

### 2.2 Format

Response status separated into some basic types and defines required and optional
keys for each type:


Type    | Description                          | Required Keys                                        | Optional Keys
------- | ------------------------------------ | ---------------------------------------------------- | -------------
success | Data submitted processed succesfully | status, message, data (invoice_number, invoice_date) | 
error   | Something went wrong                 | status, message, code                                |


#### Success

Required keys:

* `status`: Show status message. Returned value must be `success`
* `data`: Returned data from vendor, consists of:
    * `invoice_number`: Invoice number generated from vendor
    * `invoice_date`: Date where invoice generate
    * custom keys: Custom key from vendor
    
Example:

    :::json
    {
        "status": "success",
        "data": {
            "invoice_number" : "020102023",
            "invoice_date": "2014-09-09"
        }
    }
    
#### Error

Required keys:

* `status`: Show status message. Returned value must be `error`
* `code`: Error message defined from vendor. We will use this as reference for
  further investigation if API is fail
* `message`: Human readable error message

Example:

    :::json
    {
        "status": "error",
        "code": 101,
        "message": "Product code not found!"
    }



[RFC 2616]: https://www.ietf.org/rfc/rfc2616.txt
[JSON]: http://json.org/

